<?php

// Домашня сторінка
Route::get('/', function () {
    return view('index');
});

// Тестування бекенду
Route::get('/test', 'TestController@getIndex');

// API
Route::group(array('prefix' => 'api'), function() {
	Route::resource('items', 'GridController', 
		array('except' => array('create', 'edit', 'update', 'store', 'show', 'destroy')));
});