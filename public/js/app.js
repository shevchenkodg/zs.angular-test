'use strict';

angular.module('gridApp', [
	'gridApp.controllers',
	'gridApp.services'
]);