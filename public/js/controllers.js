'use strict';

angular.module('gridApp.controllers', [])

	.controller('IndexController', ['$scope', '$http', 'backendService', 'paginationService', function($scope, $http, backendService, paginationService) {

		var orderTarget, // для зберігання обраної колонки сортування
		    perPage;	// для зберігання об'єкту perPage{значення, тайтл для oprion, порядковий номер}

		// набір option для select perPage
		$scope.perpage = [{
		    value: "5",
		    title: "5",
		    id: 0
		}, {
		    value: "10",
		    title: "10",
		    id: 1
		}, {
		    value: "25",
		    title: "25",
		    id: 2
		}];

		// задаємо дефолтну кількість сторінок
		paginationService.setPerPage($scope.perpage[0]);
		
		// отримуємо об'єкт perPage
		perPage = paginationService.getPerPage($scope);
		$scope.selected = $scope.perpage[perPage.id];

		// *** Дефолтний запит на сервер для отримання даних таблиці ***
		// отримуємо дані з бекенду
		backendService.get(null, null, perPage.value, 1)
			.success(function(data) {
				$scope.items = data.data;
				// console.log(data);

				// Отримуємо масив сторінок
				paginationService.pages($scope, data.last_page);
				// Отримуємо номер попередньої сторінки
				paginationService.prevPage($scope, 1);
				// Отримуємо номер наступної сторінки
				paginationService.nextPage($scope, 1, data.last_page);

			});


		/*
		 * Функція зміни perPage
		 */
		$scope.setPaginationPerPage = function() {
			// запам'ятовуємо об'єкт новообраного perPage для його використання після перезавантаження сторінки
			paginationService.setPerPage($scope.selected);

			// дістаємо збьережений об'єкт perPage для подальшого використання
			perPage = paginationService.getPerPage($scope);


			// Отримуємо нові дані від бекенду з урахуванням нового perPage
			backendService.get(localStorage.orderTarget, localStorage.orderDirection, perPage.value, 1)
				.success(function(data) {
					$scope.items = data.data;

					// Отримуємо масив сторінок
					paginationService.pages($scope, data.last_page);
					
				});
		}


		/*
		 * Функція сортування
		 */
		$scope.orderBy = function(orderTarget) {
			// В локальне сховище я зберігаю ім'я колонки і порядок сортування
			// Ім'я колонки - щоб відловити момент, коли користувач перемикає сортування на іншу колонку
			// Напрямок сортування - щоб при повторному кліку змінювати по колу порядок сортування
			// Кожна зміна параметру - запит на сервер згідно умов тестового завдання
			
			if(localStorage.orderTarget != orderTarget) {
				// Користувач перемикає сортування на іншу колонку
				localStorage.orderTarget = orderTarget;
			}

			// Забезпечуємо зміну порядку сортування по колу (asc, desc, вимк.)
			switch (localStorage.orderDirection) {
			 	case '':
			 		localStorage.orderDirection = 'asc'
			    	break
			 	case 'asc':
			    	localStorage.orderDirection = 'desc'
			    	break
			    case 'desc':
			    	localStorage.orderDirection = ''
			    	break
			    default:
			    localStorage.orderDirection = 'asc'
			}

			// Забезпечуємо відображення іконки з Font Awesome згідно порядку сортування
			$scope.orderClass = [];
			if (localStorage.orderDirection != 'asc' && localStorage.orderDirection != 'desc') {
				$scope.orderClass[orderTarget] = {value: ''};
			}
			else {
				$scope.orderClass[orderTarget] = {value: 'fa fa-sort-' + localStorage.orderDirection};
			}
			// Запит на сервер для отримання даних таблиці з урахуванням сортування
			perPage = paginationService.getPerPage($scope);
			backendService.get(orderTarget, localStorage.orderDirection, perPage.value)
				.success(function(data) {
					$scope.items = data.data;
				});
		}


		/*
		 * Функція зміни номеру сторінки пагінації
		 */
		$scope.setPaginationPage = function(pageNumber) {

			// отримуємо об'єкт perPage
			perPage = paginationService.getPerPage($scope);
			// Запит на сервер для отримання даних таблиці з урахуванням сортування та perPage
			backendService.get(localStorage.orderTarget, localStorage.orderDirection, perPage.value, pageNumber)
				.success(function(data) {
					$scope.items = data.data;

					// Отримуємо масив сторінок
					paginationService.pages($scope, data.last_page);
					// Отримуємо номер попередньої сторінки
					paginationService.prevPage($scope, data.current_page);
					// Отримуємо номер наступної сторінки
					paginationService.nextPage($scope, data.current_page, data.last_page);

				});

		}

		/*
		 * Функція фільтру полів таблиці
		 */
		$scope.filter = function() {

			var filterData = {
				id: $scope.filterId,
				state: $scope.filterState,
				user: $scope.filterUser,
				group: $scope.filterGroup,
				role: $scope.filterRole,
				organization: $scope.filterOrganization
			};

			// отримуємо об'єкт perPage
			perPage = paginationService.getPerPage($scope);
			// отримуємо дані з бекенду
			backendService.get(localStorage.orderTarget, localStorage.orderDirection, perPage.value, 1, filterData)
				.success(function(data) {
					$scope.items = data.data;

					// Отримуємо масив сторінок
					paginationService.pages($scope, data.last_page);
					// Отримуємо номер попередньої сторінки
					paginationService.prevPage($scope, 1);
					// Отримуємо номер наступної сторінки
					paginationService.nextPage($scope, 1, data.last_page);

				});
		}
}]);